const { openFile, saveFile } = require('../services/file');

class Database{ 
    static database;
    static path = './src/database/database.json';
    constructor(){
    }
    static async setDatabase(){
        let file = await openFile(Database.path);
        if(!file){
            await saveFile(Database.path, JSON.stringify({tables: {}}));
            file = await openFile(Database.path);
        }
        console.log('[database.database-manager.setDatabase] -> file', file);
        console.log('[setDatabase] -> JSON.parse(file)', JSON.parse(file));
        Database.database = JSON.parse(file);
        return true;
    }
    async saveDatabase(database) {
        console.log('[database.database-manager.saveDatabase] -> database', database);
        await saveFile(Database.path, JSON.stringify(database));
        return true;
    }
    async getDatabase(){
        return Database.database;
    }
    async showTables(){
        const database = this.getDatabase();
        const tables = Object.keys(database);
        return tables;
    }
    async createTable(table){
        const database = await this.getDatabase();
        console.log('[database.database-manager.createTable] -> database', database);
        console.log('[database.database-manager.createTable] -> table', table);
        Object.assign(database.tables, table);
        await this.saveDatabase(database);
        return true;
    }
    async showDatabase() {
        console.log(Database.database);
    }
}

module.exports = {
    Database
}