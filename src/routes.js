const express = require('express');
const routes = express.Router();
const { body } = require('express-validator');
const scheduleController = require('./controlers/schedule');
const { validateRequestSchema } = require('../src/middleware/validator')
routes.get('/schedule', scheduleController.index);
routes.delete('/schedule/:uuid', scheduleController.remove);
routes.get('/schedule/:uuid', scheduleController.findByUUID);
routes.post('/schedule', [
    body('kind')
      .isLength({ min: 3 })
      .withMessage('must be at least 3 chars long.'),
    body('specialty')
      .not()
      .isEmpty()
      .isLength({ min: 3 })
      .withMessage('must be at least 3 chars long'),
    body('name')
      .not()
      .isEmpty()
      .isLength({ min: 5 })
      .withMessage('must be at least 5 chars long'),
    body('intervals')
      .not()
      .isEmpty()
  ],
  validateRequestSchema,
  scheduleController.save);

module.exports = routes;