const { Database } = require('../database/database-manager');
const db = new Database();
const { randomUUID } = require('crypto');

const ifExistTable = async () => {
    let database = await db.getDatabase();
    console.log('[ifExistTable] -> database', database);
    let schedulesTable = database.tables.schedules ?? undefined;
    if (!schedulesTable) {
        db.createTable({ schedules: { length: 0, data: [] }});
        database = await db.getDatabase();
        schedulesTable = database.tables.schedules
    }
    return schedulesTable
}

module.exports = {
    async insert(data) {
        const ScheduleTable = await ifExistTable();
        console.log('[repositories.schedule.insert] -> AQUI', ScheduleTable);
        ScheduleTable.data.push({
            uuid: randomUUID(),
            ...data
        });
        ScheduleTable.length += 1;
        console.log('[repositories.schedule.insert] -> ScheduleTable', ScheduleTable);
        console.log('[repositories.schedule.insert] -> db.getDatabase()', await db.getDatabase());
        await db.saveDatabase(await db.getDatabase());
        return true
    },
    async findOne(uuid) {
        const ScheduleTable = await ifExistTable();
        const result = await ScheduleTable.data.filter((value, index) => {
            return value.uuid == uuid;
        })
        if(result.length == 0 ) {
            return undefined;
        }
        return result;      
    },
    async findAll(limit, page){
        const ScheduleTable = await ifExistTable();
        if(limit, page) {
            const total = ScheduleTable.data.length;
            const rest = total % limit;
            const subtotal = total - rest;
            const totalPages = subtotal / limit + 1;

            const items = ScheduleTable.data.slice( (limit*page)-limit , limit*page );
            console.log('[repositories.schedule.findAll] -> items', items);
            return { 
                totalPages,
                items 
            }; 
        }
        return ScheduleTable.data;
    },
    async removeByUUID(uuid) {
        const ScheduleTable = await ifExistTable();
        let elementRemoved = undefined;
        const data = ScheduleTable.data.filter((value, index) => {
            if(value.uuid == uuid){
                console.log('[repositories.schedule.removeByUUID] -> index', index);
                elementRemoved = value;
            }
            return value.uuid != uuid;
        });
        if(!elementRemoved){
            return elementRemoved;
        }
        ScheduleTable.data = data;
        ScheduleTable.length -= 1;
        await db.saveDatabase(await db.getDatabase());
        return elementRemoved;
    }
}