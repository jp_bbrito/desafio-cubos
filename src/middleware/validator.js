const { validationResult } = require('express-validator');
module.exports = {
    validateRequestSchema(req, res, next) {
      const errors = validationResult(req);
      console.log('errors',errors)
      if (errors.isEmpty()) {
        return next();
      }
      return res.status(422).json(errors);
    }
}