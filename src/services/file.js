const { readFile, writeFile } = require('fs/promises');

const openFile = async ( path ) => {
    console.log('LOG: Abrindo arquivo...');
    try{
        const file = await readFile(path, 'utf8');
        return file
    } catch (error) {
        console.log(`Error: ${error}`);
        return undefined;
    }    
}

const saveFile = async (path, data) => {
    console.log('LOG: Salvando arquivo...');
    try{
        await writeFile(path, data, 'utf8');
        return true;
    } catch(error) {
        console.log(`Error: ${error}`);
        return undefined;
    }
}

module.exports = {
    openFile,
    saveFile,
}