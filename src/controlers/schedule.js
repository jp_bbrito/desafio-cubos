const { insert, findOne, findAll, removeByUUID } = require('../repositories/schedule');

module.exports = {
    async index(req, res) {
       const { limit, page } = req.query;
       console.log('[controllers.schedule.index] -> limit, query', limit, page);
        const result = await findAll(limit, page);
        if(!result){
            return res.status(404).json({msg: 'item not found!'});
        }
        return res.json(result);
    },
    async remove(req, res) {
        const { uuid } = req.params;
        const result = await removeByUUID(uuid);
        if(!result){
            return res.status(404).json({msg: 'item not found!'});
        }
        return res.json(result);
    },
    async findByUUID(req, res) {
        const { uuid } = req.params;
        const result = await findOne(uuid);
        if(!result){
            return res.status(404).json({msg: 'item not found!'});
        }
        return res.json(result[0]);
    },
    async save(req, res) {
        const { body } = req;
        console.log('[Body] -> ', body);
        await insert(body);
        return res.status(201).json(body);
    }
}