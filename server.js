const express = require('express');
const PORT = 3000;
const app = express();
const { Database } = require('./src/database/database-manager');
Database.setDatabase();
app.use(express.json());
app.use('/api', require('./src/routes'));

app.listen(PORT, () => {
    console.log('[LOG] Server working...');
});