# DESAFIO

As collection do Postman estão no arquivo "DESAFIO-CUBOS.postman_collection.json"

# Exemplo de payloads

### Dia unico.
```
{
    "kind": "day",
    "date": "25-11-2018",
    "specialty": "doctor",
    "name": "Ben Carson",
    "intervals": [
        {
            "start": "08:00",
            "end": "09:00"
        }
    ]
}
```

###  Semanal.
```
{
    "kind": "weekly",
    "day": "tuesday",
    "specialty": "doctor",
    "name": "Thomas Sowell",
    "intervals": [
        {
            "start": "10:00",
            "end": "11:00"
        },
        {
            "start": "14:00",
            "end": "18:00"
        }
    ]
}
```

###  Diariamente.
```
{
    "kind": "daily",
    "specialty": "doctor",
    "name": "Dr. Friedman",
    "intervals": [
        {
            "start": "10:00",
            "end": "11:00"
        },
        {
            "start": "14:00",
            "end": "18:00"
        }
    ]
}
```
